<script type="text/javascript">
	window.onload = function()
	{
		var editor = CKEDITOR.replace('description',
      {


        toolbar :
            [
                { name: 'document', items : [ 'Preview','Source' ] },
                { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
              
                { name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'] },
                        '/',
                { name: 'styles', items : [ 'Styles','Format' ] },
                { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-' ] },
                { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
                { name: 'tools', items : [ 'Maximize','-','About' ] },
				{ name: 'document', items : [ 'JustifyBlock' ] }

        /*width: "450px"*/


        });
		 CKFinder.setupCKEditor( null, '<?php echo base_url(); ?>assets/ckfinder/' );
	};
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
    
   
    
    
    
	 jQuery('.deleterow').click(function(){
    var conf = confirm('Continue delete?');
    if(conf)
	jQuery(this).parents('tr').fadeOut();
    var idVal=jQuery(this).parents('tr').find('#rec_id').val();
      jQuery.ajax({
					'url' : "<?php echo site_url('c_products/delete_products_by_id'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'id' : idVal},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					var container = jQuery('#container'); //jquery selector (get element by id)
					if(data){
					
					}
					}
					}); 
    return false;
    }); 
/*base url*/	
	function GetBaseUrl() {
    try {
        var url = location.href;

        var start = url.indexOf('//');
        if (start < 0)
            start = 0 
        else 
            start = start + 2;

        var end = url.indexOf('/', start);
        if (end < 0) end = url.length - start;

        var baseURL = url.substring(start, end);
        return baseURL;
    }
    catch (arg) {
        return null;
    }
}
/*base url*/	
	jQuery('.editrow').click(function(){
	var idVal=jQuery(this).parents('tr').find('#rec_id').val();
  jQuery.ajax({
					'url' : "<?php echo site_url('c_products/get_products_by_id'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'id' : idVal},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					var container = jQuery('#container'); //jquery selector (get element by id)
					if(data){
					
					jQuery('#title').val(data.title);
					jQuery('#w_price').val(data.w_price);
					jQuery('#r_price').val(data.r_price);
					jQuery('#stock').val(data.stock);
					jQuery('#item_code').val(data.item_code);
					jQuery('#sc_code').val(data.sc_code);
					jQuery('#description').val(data.description);
					//CKEDITOR.instances.description.setData(data.description);
					
					jQuery('span#img1').html('<img src="'+data.url+'uploads/img/'+ data.img1+'" height="100" />');
					jQuery('span#img2').html('<img src="'+data.url+'uploads/img/'+ data.img2+'" height="100" />');
					jQuery('span#img3').html('<img src="'+data.url+'uploads/img/'+ data.img3+'" height="100" />');
					
					
					
					jQuery('#action').val('edit');
					jQuery('#language').val(data.language);
					jQuery('#p_id').val(data.id);
					}
					}
					}); 
 jQuery("html, body").animate({scrollTop: 100}, 1000, 'swing');
  
    return false;
    }); 
	
	
        // dynamic table
        jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            "aaSortingFixed": [[0,'asc']],
            "fnDrawCallback": function(oSettings) {
                jQuery.uniform.update();
            }
        });
        
        jQuery('#dyntable2').dataTable( {
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "300px"
        });
        
    });
	
</script>
<script type="text/javascript">

    jQuery(document).ready(function(){

        jQuery("input[name='clanguage']").click(function(){
            var radioValue = jQuery("input[name='clanguage']:checked").val();
			jQuery('#form_language').submit();
        });
    });

</script>
<!--************************************BELOW AREA MANDATORY FOR ALL PAGES *********************************************************************************88--> 
 <div class="rightpanel">
        
        <ul class="breadcrumbs">
            
            <li>Add Profile Data</li>
        </ul>		
			
 <div class="pageheader">
           <!-- <form action="results.html" method="post" class="searchbar">
                <input type="text" name="keyword" placeholder="To search type and hit enter..." />
            </form>-->
            <div class="pageicon"><span class="iconfa-pencil"></span></div>
            <div class="pagetitle">
                <h5>Manage</h5>
                <h1>Profile</h1>
            </div>
        </div><!--pageheader-->
  <!--alert section-->
   
   <?php if($this->session->flashdata('error_messages')){ ?>
		<div class="alert alert-error">
		<button data-dismiss="alert" class="close" type="button">&times;</button>
		<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error_messages');?>
		</div><!--alert-->
    
   <?php } ?> 
			
 <?php if($this->session->flashdata('success_messages')){ ?>
		<div class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">&times;</button>
		<strong>Well done!</strong> <?php  echo $this->session->flashdata('success_messages'); ?>
		</div><!--alert-->
   <?php } ?> 			
			
			
			<!--<div class="alert alert-info">
			  <button data-dismiss="alert" class="close" type="button">&times;</button>
			  <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
			</div>--><!--alert-->     
  <!--alert section--> 
        <div class="maincontent">   <!--**********************This DIV Close Inside Footer***************************-->
            <div class="maincontentinner"> <!--**********************This DIV Close Inside Footer***************************-->
			
<!--************************************ABOVE AREA MANDATORY FOR ALL PAGES ***********************************************************************************-->


<?php
if($profile)
							{
							foreach($profile as $row)
							{
							$id = $row['id'];	
							$name = $row['name'];
							$work = $row['work'];
							$college = $row['college'];
							$school = $row['school'];
							$music = $row['music'];
							$movies = $row['movies'];
							$sports = $row['sports'];
							$img1 = $row['img1'];
							$img2 = $row['img2'];
							}
							}
?>


<div class="widgetbox box-inverse">
                <h4 class="widgettitle">Profile</h4>
                <div class="widgetcontent wc1">
				 
                    <form id="form_profile" class="stdform" method="post"  enctype="multipart/form-data" action="<?php echo base_url('c_form/process');?>">
						 <div class="par control-group">
                                    <label class="control-label" for="title">Your Name</label>
                                <div class="controls"><input type="text" name="name" id="name" class="input-large" value="<?php if(isset($name)) { echo $name; }  ?>" /></div>
                            </div>
                            
                            
						 <div class="par control-group">
                                    <label class="control-label" for="summery">Work</label>
                                <div class="controls"><input type="text" name="work" id="work" class="input-large" value="<?php if(isset($work)) { echo $work; }  ?>" /></div>
                            </div>
						
                              
							  <div class="par control-group">
                                    <label class="control-label" for="summery">College</label>
                                <div class="controls"><input type="text" name="college" id="college" class="input-large" value="<?php if(isset($college)) { echo $college; }  ?>" /></div>
                            </div>
							
							
							<div class="par control-group">
                                    <label class="control-label" for="state">School</label>
                                <div class="controls"><input type="text" name="school" id="school" class="input-large" value="<?php if(isset($school)) { echo $school; }  ?>" /></div>
                            </div>  
                            
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state">Music</label>
                                <div class="controls"><input type="text" name="music" id="music" class="input-large" value="<?php if(isset($music)) { echo $music; }  ?>" /></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state">Movies</label>
                                <div class="controls"><input type="text" name="movies" id="movies" class="input-large" value="<?php if(isset($movies)) { echo $movies; }  ?>" /></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state">Sports</label>
                                <div class="controls"><input type="text" name="sports" id="sports" class="input-large" value="<?php if(isset($sports)) { echo $sports; }  ?>" /></div>
                            </div>
                            
							  
							
							<div class="par control-group">
                                    <label class="control-label" for="country">Profile Picture</label>
                                <div class="controls">
								<div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="input-append">
				<div class="uneditable-input span3">
				    <i class="iconfa-file fileupload-exists"></i>
				    <span class="fileupload-preview"></span>
				</div>
				<span class="btn btn-file"><span class="fileupload-new">Select file</span>
				<span class="fileupload-exists">Change</span>
				<input type="file" name="img1" id="img1" /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				<?php
				if(isset($img1)) {
					?>
				<span id="img1"  style="height:100px; overflow:hidden; width:150px; display:block; margin:5px;"><img src="<?php echo base_url("uploads/img/$img1");?>" height="100" /></span>
				<?php
				}
				?>
				</div>
			    </div>
								</div>
                            </div> 
							<div class="par control-group">
                                    <label class="control-label" for="country">Cover Photo</label>
                                <div class="controls">
								<div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="input-append">
				<div class="uneditable-input span3">
				    <i class="iconfa-file fileupload-exists"></i>
				    <span class="fileupload-preview"></span>
				</div>
				<span class="btn btn-file"><span class="fileupload-new">Select file</span>
				<span class="fileupload-exists">Change</span>
				<input type="file" id="img2" name="img2" /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				<?php
				if(isset($img2)) {
					?>
				<span id="img2"  style="height:100px; overflow:hidden; width:150px; display:block; margin:5px;">
				<img src="<?php echo base_url("uploads/img/$img2");?>" height="100" /></span>
				<?php
				}
				?>
				</div>
			    </div>
								</div>
                            </div>    
							 
							
                           <p class="stdformbutton">
						   <?php
							if($profile)
							{
								
							?>
                                    <button class="btn btn-primary">Update Profile</button>
							<?php
							}
							else
							{
							?>
								<button class="btn btn-primary">Add Profile</button>
						<?php
							}
						?>								
									<?php
							if($profile)
							{
								
							?>
							
							<a href="<?php echo base_url('c_home/profile');?>" class="btn btn-primary">View Profile</a>
								<?php
							}
							
							?>	
                            </p>
							<?php
							if($profile)
							{
							foreach($profile as $row)
							{
							$id = $row['id'];	
							}
							?>
							<input type="hidden" name="action" id="action" value="<?php echo('edit');?>"   />
							<input type="hidden" name="rec_id" id="rec_id" value="<?php echo $id;?>"   />
							<?php
							}
							else
							{
								
							?>
							<input type="hidden" name="action" id="action" value="<?php echo('add');?>"   />
							<?php
							}
							?>
                    </form>
                </div><!--widgetcontent-->
            </div>
			 
					
							
                
                      <div class="divider30"></div>
                
  