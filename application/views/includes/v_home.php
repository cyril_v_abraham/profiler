  <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dialogextend.js"></script>
  <style id="jsbin-css">
 
  .ui-dialog { font-size: 12px; }
  /***** HEADER *****/
  header { background-color: #f0f0f0; border-radius: 1em; box-shadow: inset 0 0 10px gray; padding: 1em 1.5em 1.5em 1.5em; }
  header h1 { margin: 0 0 0.5em 0; }
  header ul { margin: 1em 0 0 0; }
  /***** CONTENT *****/
  section fieldset { margin: 5px; width: 200px; }
  section label { cursor: pointer; }
  #config-icon .wrapper { clear: both; }
  #config-icon ins { float: left; margin: 0 5px 0 0; }
  #config-icon label { float: left; }
  #config-icon select { float: right; width: 100px; }
  #config-method button { width: 48%; }
  /***** FOOTER *****/
  footer { clear: both; padding-top: 2em; }
  footer button { background-color: #e0e0e0; border: none; border-radius: 1em; box-shadow: 0 5px 5px silver; cursor: pointer; font-size: 200%; padding: 10px 0 10px 0; text-align: center; width: 10em; }
  footer button:hover { background-color: #d0d0d0; box-shadow: 0 5px 5px #aaaaaa; }
  footer button:active { box-shadow: 0 4px 4px #aaaaaa; position: relative; top: 1px; }
  </style>
  
  <?php /*?><link href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" /><?php */?>
  <script>

  </script>
  
  
  
<script type="text/javascript">
    jQuery(document).ready(function(){
	var terminal_cnt_int = parseInt(jQuery('#terminal_cnt').val());
	jQuery(function(){
  var last;

  // preview icon
  jQuery("#config-icon select")
    .change(function(){
      var icon = "<span class='ui-icon "+jQuery(this).val()+"'></span>";
      jQuery(this).parents(".wrapper").find("ins").html(icon);
    })
    .trigger("change");


  // click to open dialog
  jQuery("#new-dialog").click(function(){
    //dialog options
	var terminal_cnt = parseInt(jQuery('#terminal_cnt').val()) + 1;
	var title = "TERMINAL " + terminal_cnt;
	//jQuery('#terminal_cnt').val(terminal_cnt);
	
	
    var dialogOptions = {
      "title" : title,
      "width" : 600,
      "height" : 400,
      "modal" : false,
      "resizable" : true,
      "draggable" : true,
      "close" : function(){
        if(last[0] != this){
          $(this).remove(); 
        }
      }
    };

    // dialog-extend options
    var dialogExtendOptions = {
      "closable" : true,
      "maximizable" : true,
      "minimizable" : true,
      "collapsable" :  false
    };
    jQuery("#my-form [name=icon]").each(function(){
      if ( jQuery(this).find("option:selected").html() != "(default)" ) {
        dialogExtendOptions.icons = dialogExtendOptions.icons || {};
        dialogExtendOptions.icons[$(this).attr("rel")] = jQuery(this).val();
      }
    });
    jQuery("#my-form [name=event]").each(function(){
      if ( jQuery(this).is(":checked") ) {
        dialogExtendOptions[jQuery(this).attr("rel")] = function(evt, a,b,c) {
          jQuery(evt.target).prepend("Hello Team<br />");
        };
      }
    });
    // open dialog
    last = jQuery("<div />").dialog(dialogOptions).dialogExtend(dialogExtendOptions);
  });
  
  //click to reopen dialog
  jQuery('#reopen-dialog').click(function(){
    last.dialog('open');
  });
  // click to invoke method
  jQuery("#config-method button").click(function(){
    var command = jQuery(this).text();
    var dialog = jQuery(".ui-dialog:last").find(".ui-dialog-content");
    if ( jQuery(dialog).length ) {
      if ( command == 'state' ) {
        alert( jQuery(dialog).dialogExtend(command) );
      } else {
        jQuery(dialog).dialogExtend(command);
      }
    }
  });
jQuery('body').on('mouseover','.ui-dialog-content',function(){
    jQuery('.ui-dialog-content').css('border','none');
    <!--$(this).css('border','5px solid #000');-->
    
})

jQuery("#new-dialog").click(function(){


var terminal_cnt = parseInt(jQuery('#terminal_cnt').val()) + 1;
jQuery('#terminal_cnt').val(terminal_cnt);
    
    jQuery('.ui-dialog-content:last').append('<div id="result'+terminal_cnt+'" style="width: 100%; font-family: Courier New; font-size: 10pt; color: #000000; border: 0px none; height: auto; background-color: #a8d9ff; box-sizing:border-box; padding:0px 5px 0px 5px; text-transform: uppercase;font-weight:bold;" name="result"></div><div class="noti"><div class="prompt">></div><input id="command'+terminal_cnt+'" class="command" type="text" style="font-family: Courier New; background-color: #a8d9ff; color: #000; border: none; height:31px;float:left;box-sizing:border-box;" autocomplete="off" name="command"></div><input type="hidden" name="token" id="token'+terminal_cnt+'"/><input type="hidden" name="auth" id="auth'+terminal_cnt+'" />');
	
	
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/open_connection'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					if(data){
					jQuery('#token'+terminal_cnt).val(data.token);
					jQuery('#auth'+terminal_cnt).val(data.auth);
					}
					}
	
	});
	capitalise(terminal_cnt);
	keypress(terminal_cnt);
	
	
    
    })
	
/*$("body").on('blur','.command',function(){
    $(this).closest('.ui-dialog-content').append('Your content here '+$(this).val());
	
    })*/
    
});
	
	
	//jQuery( "#command" ).focus();
	jQuery('body').on('click','.fullscreen',function(){
  
 	var i = document.getElementById("contents");
 
// go full-screen
	if (i.requestFullscreen) {
    i.requestFullscreen();
	} else if (i.webkitRequestFullscreen) {
    i.webkitRequestFullscreen();
	} else if (i.mozRequestFullScreen) {
    i.mozRequestFullScreen();
	} else if (i.msRequestFullscreen) {
    i.msRequestFullscreen();
	}
  	})
	capitalise(terminal_cnt_int);
	
	function capitalise(terminal_cnt_int)
	{
	jQuery('#command'+terminal_cnt_int).bind('keyup', function (e) {
    if (e.which >= 97 && e.which <= 122) {
        var newKey = e.which - 32;
        // I have tried setting those
        e.keyCode = newKey;
        e.charCode = newKey;
    }
    jQuery('#command'+terminal_cnt_int).val((jQuery('#command'+terminal_cnt_int).val()).toUpperCase());
	});
	}
	

	 jQuery("#tgg_btn").click(function(){
        jQuery(".dvCommandsList").slideToggle();
    });
	
	jQuery('body').on('click','.opencon',function () {
	
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/open_connection'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					if(data){
					//alert(data.connection_status);
					jQuery('#command'+terminal_cnt_int ).focus();
					jQuery('#result' ).empty();
					jQuery('#token'+terminal_cnt_int).val(data.token);
					jQuery('#auth'+terminal_cnt_int).val(data.auth);
					jQuery("#opencon").html('Close Connection');
					jQuery('#opencon').attr("id","closecon");
					jQuery('.opencon').addClass('closecon').removeClass('opencon');
					}
					}
	
	});
	
	jQuery('.opencon').addClass('closecon').removeClass('opencon');
	});
	
	jQuery('body').on('click','.closecon',function () {
	
	var token=jQuery('#token'+terminal_cnt_int).val();
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/close_connection'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'token' : token},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					if(data){
					//alert(data.connection_status);
					jQuery( "#result" ).empty();
					jQuery( "#result" ).append("Welcome to GDS Terminal!    Click Open Connection to proceed");
					//jQuery('#token').val("");
					//jQuery('#auth').val("");
					jQuery("#closecon").html('Open Connection');
					jQuery('#closecon').attr("id","opencon");
					jQuery('.closecon').addClass('opencon').removeClass('closecon');
					}
					}
	
	});
	
	});
	
	jQuery('body').on('click','.tktbtn',function () {
	var pnr=jQuery('#pnr').val();
	var token=jQuery('#token').val();
    var auth=jQuery('#auth').val();
	var con = "*";
	var cmd=con + pnr;
	//alert(cmd);
	
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/getresult'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'cmd' : cmd,'token' : token,'auth' : auth},
					beforeSend: function(){
					//jQuery('#result').val("");
					jQuery( "#result" ).empty();
           jQuery("#command").val("Please Wait...");
        },
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					jQuery('#command').val("");
					
					if(data){
					//jQuery('#result').val(data);
					
					var errorStr = data.out;

					if(errorStr.contains("Session May Have Timed Out") || errorStr.contains("HTTP Status 500")){
    				jQuery( "#result" ).append("<br/> <br/>Session May Have Timed Out,Please Open Connection");
					
					var token=jQuery('#token').val();
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/close_connection'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'token' : token},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					if(data){
					//jQuery( "#result" ).empty();
					//jQuery( "#result" ).append("Welcome to GDS Terminal!    Click Open Connection to proceed");
					//jQuery('#token').val("");
					//jQuery('#auth').val("");
					jQuery("#closecon").html('Open Connection');
					jQuery('#closecon').attr("id","opencon");
					jQuery('.closecon').addClass('opencon').removeClass('closecon');
					}
					}
	
	});
					
					}
					/*else if(errorStr.contains("HTTP Status 500"))
					{
					jQuery( "#result" ).append("<br/> <br/>Session May Have Timed Out,Please Open Connection");
					}*/
					else
						{
					jQuery( "#result" ).append(data.out+"<br/> <br/>");
					jQuery( "#command" ).focus();
					jQuery('#contents').animate({
        scrollTop: jQuery('#contents')[0].scrollHeight}, 2000);
						}
					
					}
					}
					}); 
	
	});
	
	keypress(terminal_cnt_int);
	
	function keypress(terminal_cnt_int)
	{
	jQuery('#command'+terminal_cnt_int).keypress(function (e) {
 var key = e.which;
 var cmd=jQuery('#command'+terminal_cnt_int).val();
 if(key == 13 && cmd != "")  // the enter key code
  {
   var token=jQuery('#token'+terminal_cnt_int).val();
   var auth=jQuery('#auth'+terminal_cnt_int).val();
   
   <?php /*?>var token= "<?php echo $token; ?>"; 
   var auth= "<?php echo $auth; ?>";<?php */?>
   
   jQuery.ajax({
					'url' : "<?php echo site_url('c_home/getresult'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'cmd' : cmd,'token' : token,'auth' : auth},
					beforeSend: function(){
					
					//jQuery( "#result" ).empty();
           jQuery('#command'+terminal_cnt_int).val("Please Wait...");
        },
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					jQuery('#command'+terminal_cnt_int).val("");
					
					if(data){
					var errorStr = data.out;

					if(errorStr.contains("Session May Have Timed Out") || errorStr.contains("HTTP Status 500")){
    				jQuery( "#result" ).append("<br/> <br/>Session May Have Timed Out,Please Open Connection");
					
					var token=jQuery('#token'+terminal_cnt_int).val();
	jQuery.ajax({
					'url' : "<?php echo site_url('c_home/close_connection'); ?>",
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'token' : token},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					if(data){
					//jQuery( "#result" ).empty();
					//jQuery( "#result" ).append("Welcome to GDS Terminal!    Click Open Connection to proceed");
					//jQuery('#token').val("");
					//jQuery('#auth').val("");
					jQuery("#closecon").html('Open Connection');
					jQuery('#closecon').attr("id","opencon");
					jQuery('.closecon').addClass('opencon').removeClass('closecon');
					}
					}
	
	});
					
					}
					/*else if(errorStr.contains("HTTP Status 500"))
					{
					jQuery( "#result" ).append("<br/> <br/>Session May Have Timed Out,Please Open Connection");
					}*/
					else
						{
					jQuery('#result'+terminal_cnt_int).append(data.out+"<br/> <br/>");
					jQuery('#command'+terminal_cnt_int).focus();
					jQuery('#contents').animate({
        scrollTop: jQuery('#contents')[0].scrollHeight}, 2000);
						}

					}
					}
					}); 
  
  }
   });
	}
	
	
	
   
   jQuery('.dv_commandItem').click(function(e) {
			e.preventDefault();
			//alert(jQuery(this).text());
			jQuery('#command'+terminal_cnt_int).val("");
			jQuery('#command'+terminal_cnt_int).val(jQuery(this).text());
			jQuery('#command'+terminal_cnt_int).focus();
			
});

	});
	
</script>


<?php include_once('includes/header.php');
$session_data = $this->session->userdata('connection_status');
?>
<div class="leftpanel">
        
        <div class="leftmenu">        
           
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel" style="margin-left:0px;">
        
       
        <div class="pageheader">
            
            <!--<div class="pageicon"><span class="iconfa-laptop"></span></div>-->
            <div class="pagetitle span6">
               <!-- <h5>All Features Summary</h5>-->
                <h1>Galileo Web</h1>
            </div>
			<div class="span3">
				<div class="dv_Right" style="float:left;">
  
  <?php
  
  if($session_data['connection_status'])
  {
  if($session_data['connection_status'] == "Close Connection")
  {
  $sta = "closecon";
  }
  else
  {
  $sta = "opencon";
  }
  ?>
  <button class="btn btn-primary <?php echo $sta; ?>" id="<?php echo $sta; ?>"><?php echo $session_data['connection_status']; ?></button>
  
  <?php
  }
  else
  {
  ?>
  <button class="btn btn-primary opencon" id="opencon">Open Connection</button>
  <?php
  }
  ?>
  
  <div class="Commands" style="width:100%;float:left;">
  <div class="dv_title" id="tgg_btn" style="cursor:pointer;">Command History
  <?php
  if($Role_id == "3")
  {
  ?>
	<span style="padding-left:5%; float:right;"><a href="<?php echo site_url('c_commands/allcommands'); ?>">View All</a></span>				
  <?php
	}
  ?> 
  </div>
    

  <div class="dvCommandsList" style="display:none;">
  <div class="commandList">
  <ul class="ul_commandHis">
  <?php
  if($cmd_history)
  {
  foreach($cmd_history as $row)
  {
  ?>
  <li class="dv_commandItem"><a class="a_command" href="#"><?php echo $row['Command']; ?></a></li>
  <?php
  }
  }
  ?>
  </ul>
  </div>
  </div>
  
  </div>
  
  
  
  </div>
			</div>
			<div class="span3">
				<input type="text" name="pnr" id="pnr" placeholder="Enter PNR"/>
	<button class="btn btn-primary tktbtn" id="tktbtn">Retrieve Ticket</button>
			</div>
			
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                    <div id="dashboard-left" class="span8">
                     
					 <div class="span12">
  <div id="dv_Left" style="width:100%;float:left;">
  <span class="btn btn-primary new-dialog" id="new-dialog" >Open New Terminal</span>
  
  <div id="contents" style="width: 100%; font-family: Courier New; font-size: 10pt; color: #000000; border: 0px none; height: 483px; background-color: #a8d9ff; box-sizing:border-box; padding:0px 0px 0px 0px; text-transform: uppercase;font-weight:bold; border:solid 1px #3486c5; overflow:auto;">
  
  <div class="dv_terminal"><span class="iconfa-laptop"></span> Terminal 1
  <span class="iconfa-resize-full fullscreen" ></span>
  </div>
  
      <div id="result1" style="width: 100%; font-family: Courier New; font-size: 10pt; color: #000000; border: 0px none; height: auto; background-color: #a8d9ff; box-sizing:border-box; padding:0px 5px 0px 5px; text-transform: uppercase;font-weight:bold;" name="result">
	  <?php
	   if(!$session_data['connection_status'])
  {
	  echo "Welcome to GDS Terminal!    Click Open Connection to proceed";
	}?>  
	  </div>
	  
	  <div id="noti">
	  <div class="prompt">></div>
	  <input id="command1" type="text" style="font-family: Courier New; background-color: #a8d9ff; color: #000; border: none; height:31px;float:left;box-sizing:border-box;" autocomplete="off" name="command" class="command">
	  </div>
	  
      <div class="clear"></div>
    </div>
    
	
	
	
  <input type="hidden" name="token" id="token1" />
  <input type="hidden" name="auth" id="auth1" />
  <input type="hidden" name="connection_status" id="connection_status1" />
  <input type="hidden" name="terminal_cnt" id="terminal_cnt" value="1" />
  </div>
  
</div>


                       
                    </div><!--span8-->
                     
                    <div id="dashboard-right" class="span4">
					<?php
					if($Role_id == "3")
					{
					 include_once('includes/rightpanel.php');
					}
					?> 
					 
                        <h4 class="widgettitle">Calendar</h4>
                        <div class="widgetcontent nopadding">
                            <div id="datepicker"></div>
                        </div>
                        
                        <!--tabbedwidget-->
						
						 <div class="widgetbox">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <!--<button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>-->
                            </div>
                            <h4 class="widgettitle">About the Product</h4>
                        </div>
                        <div class="widgetcontent">
                            Cax-Terminal is a powerful tool designed and developed by Caxita to make the terminal accessibility a greater and richer experience compared to the conventional system. This web based system allows the user to access the terminal through laptop, desktop, tablet or mobile anywhere anytime. The system is protected by unique username and password, so security is guaranteed. 
                        </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <br />
                                                
                    </div><!--span4-->
                </div><!--row-fluid-->
			