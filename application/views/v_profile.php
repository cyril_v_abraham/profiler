<!--************************************BELOW AREA MANDATORY FOR ALL PAGES *********************************************************************************88--> 
 <div class="rightpanel">
    <?php
if($profile)
{
	foreach($profile as $row)
							{
							$name = $row['name'];
							$work = $row['work'];
							$college = $row['college'];
							$school = $row['school'];
							$music = $row['music'];
							$movies = $row['movies'];
							$sports = $row['sports'];
							$img1 = $row['img1'];
							$img2 = $row['img2'];							
							}
}
?>    
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url('c_home/profile');?>"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Profiler</li>
            <!--<li class="right">
                    <a  data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                    <ul class="dropdown-menu pull-right skin-color">
                        <li><a href="default">Default</a></li>
                        <li><a href="navyblue">Navy Blue</a></li>
                        <li><a href="palegreen">Pale Green</a></li>
                        <li><a href="red">Red</a></li>
                        <li><a href="green">Green</a></li>
                        <li><a href="brown">Brown</a></li>
                    </ul>
            </li>-->
        </ul>	
 <div class="pageheader">
            
            <div class="pageicon"><span class="iconfa-user"></span></div>
            <div class="pagetitle">
                <h5>&nbsp;</h5>
                <h1><?php if(isset($name)) { echo $name; } ?></h1>
				<?php
				if(isset($img2))
				{
				?>
			  <img src="<?php echo base_url("uploads/img/$img2");?>" alt="" style="height: 315px;" />
			  <?php
				}
				else
				{
			  ?>
			  <img src="<?php echo base_url("assets/images/photos/no_cover.jpg");?>" alt="" />
			  <?php
				}
			  ?>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">   <!--**********************This DIV Close Inside Footer***************************88-->
            <div class="maincontentinner"> <!--**********************This DIV Close Inside Footer***************************88-->
			
<!--************************************ABOVE AREA MANDATORY FOR ALL PAGES *********************************************************************************88-->


<div class="widgetbox box-inverse">
                <h4 class="widgettitle">About</h4>
                <div class="widgetcontent wc1">
				<span class="stdform1">
				<div class="par control-group">
                                    <label class="control-label" for="summery"><span class="iconfa-suitcase"></span> Work</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($work)) { echo $work; }  ?></h5></div>
                            </div>
						
                              
							  <div class="par control-group">
                                    <label class="control-label" for="summery"><span class="iconfa-book"></span> College</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($college)) { echo $college; }  ?></h5></div>
                            </div>
							
							
							<div class="par control-group">
                                    <label class="control-label" for="state"><span class="iconfa-book"></span> School</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($school)) { echo $school; }  ?></h5></div>
                            </div>  
                            
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state"><span class="iconfa-music"></span> Music</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($music)) { echo $music; }  ?></h5></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state"><span class="iconfa-film"></span> Movies</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($movies)) { echo $movies; }  ?></h5></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="state"><span class="iconfa-trophy"></span> Sports</label>
                                <div class="controls"><h5 class="subtitle"><?php if(isset($sports)) { echo $sports; }  ?></h5></div>
                            </div>
							</span>
				</div>
				</div>



               