<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
  
class M_form extends CI_Model {
 
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function getAllProfile() {
				$this->db->select('*');
				$this->db->from('profile');
				$query = $this->db->get();
				return $query->result_array();
				}
	
	
			
				
	function processData($frmData){
		$data = array(
		'name'	     => $frmData['name'] ,
		'work'      =>  $frmData['work'] ,
		'college'      =>  $frmData['college'] ,
		'school'      => $frmData['school'] ,
		'sports' =>  $frmData['sports'] ,
		'music'     => $frmData['music'] ,
		'movies'        =>  $frmData['movies']
		);
		if(!empty($frmData['img1'])){
		$data['img1']=$frmData['img1'];
		}
		if(!empty($frmData['img2'])){
		$data['img2']=$frmData['img2'];
		}
		
		
		
		if($frmData['action']=='add'){
		$this->db->insert('profile', $data);
		if($this->db->affected_rows() > 0)
		{
			$status="true"; // to the controller
		} else{
		$status="false";
		}
		}elseif($frmData['action']=='edit'){
		$this->db->where('id', $frmData['p_id']);
		$this->db->update('profile', $data); 
		if($this->db->affected_rows() > 0)
		{
			$status="true"; // to the controller
		} else{
		$status="false";
		}
		}
	return $status;
	}
	
	
				
	function get_profile_by_id($id) {
				$this->db->select('*');
				$this->db->from('profile');
				$this->db->where('id', $id);
				$query = $this->db->get();
				return $query->result_array();
				}
	
	
}
  
/* End of file m_login.php */
/* Location: ./application/models/admin/m_login.php */