<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_form extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->model('m_form','',TRUE);
        $this->load->helper('url');
        $this->load->library(array('form_validation','session'));
    }
 
    function index() {
        
			$result['profile']=$this->m_form->getAllProfile();
			$this->load->view('includes/header_form');
            $this->load->view('v_form',$result);
			$this->load->view('includes/footer');
        
    }
 function process(){
 
		$frmData=$_REQUEST;
		$this->form_validation->set_rules('name', 'Your Name', 'required|min_length[1]|max_length[1000]');
		if ($this->form_validation->run() == TRUE)
		{
			/*array config*/
		
		$data = array(
		'name'	     => $frmData['name'] ,
		'work'      =>  $frmData['work'] ,
		'college'      =>  $frmData['college'] ,
		'school'      => $frmData['school'] ,
		'sports' =>  $frmData['sports'] ,
		'music'     => $frmData['music'] ,
		'movies'        =>  $frmData['movies'] ,
		'action'  => $frmData['action']
		);
		//$data['p_small_img']='';
		if(!empty($_FILES['img1']['name'])){
		$config1['upload_path'] = './uploads/img/';
		$config1['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
		//$config['encrypt_name'] = TRUE;
		$config1['overwrite'] = FALSE;
		/*$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';*/
		$this->load->library('upload');
		$this->upload->initialize($config1);
		$this->upload->do_upload('img1');
		$file_data1 = $this->upload->data();
		$data['img1']=$file_data1['file_name'];
		}
		
		//$data['p_detail_img']='';
		if(!empty($_FILES['img2']['name'])){
		$config2['upload_path'] = './uploads/img/';
		$config2['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
		//$config['encrypt_name'] = TRUE;
		$config2['overwrite'] = FALSE;
		/*$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';*/
		$this->load->library('upload');
		$this->upload->initialize($config2);
		$this->upload->do_upload('img2');
		$file_data2 = $this->upload->data();
		$data['img2']=$file_data2['file_name'];
		}
		
		
		if($frmData['action']=="add"){
		//echo('<pre>');print_r($data);echo('</pre>');exit;
		$status=$this->m_form->processData($data);
		if($status=='true'){
		$this->session->set_flashdata('success_messages', 'Record added successfully...');
		
		}else{
		$this->session->set_flashdata('error_messages', 'The process faild to execute try again...');
		
		}
		}elseif($frmData['action']=="edit"){
		$data['p_id']=$frmData['rec_id'];
		/*remove old images, if add new one*/
		    $res_array=$this->m_form->get_profile_by_id($data['p_id']);
			
			if($res_array){
			
			if(isset($data['img1'])){
			$thumb_url=$res_array[0]['img1'];
			@unlink('uploads/img/'.$thumb_url);
			}
			if(isset($data['img2'])){
			$detail_url=$res_array[0]['img2'];
			@unlink('uploads/img/'.$detail_url);
			}
			
			
			}
		/*remove old images, if add new one*/
		$status=$this->m_form->processData($data);
		if($status=='true'){
		$this->session->set_flashdata('success_messages', 'Record updated successfully...');
		
		}else{
		$this->session->set_flashdata('error_messages', 'The process faild to execute try again...');
		
		}
		}
		}else{
		$this->session->set_flashdata('error_messages', validation_errors());
		
		}
redirect( 'C_form' );

		
}

 function delete_products_by_id(){
 if($this->session->userdata('logged_in'))
        {
			$id=$_REQUEST['id'];
			$res_array=$this->m_form->get_products_by_id($id);
			if($res_array){
			if($res_array[0]['img1']){
			$thumb_url=$res_array[0]['img1'];
			@unlink('uploads/img/'.$thumb_url);
			}
			if($res_array[0]['img2']){
			$detail_url=$res_array[0]['img2'];
			@unlink('uploads/img/'.$detail_url);
			}
			
			if($res_array[0]['img3']){
			$banner_url=$res_array[0]['img3'];
			@unlink('uploads/img/'.$banner_url);
			}
			}
 			$this->m_form->del_products_by_id($id);
			exit;
			} else {
            redirect('en/admin/c_login', 'refresh');
        }
 }
 
  function get_products_by_id(){
 if($this->session->userdata('logged_in'))
        {
			header('Content-Type: application/json');
			$id=$_REQUEST['id'];
 			$result=$this->m_form->get_products_by_id($id);
			$this->row= new stdClass();
			$this->row->id=$result[0]['product_id'];
			$this->row->title=$result[0]['name'];
			$this->row->maincat=$result[0]['category_id'];
			$this->row->subcategory_id=$result[0]['subcategory_id'];
			$this->row->parent_id=$result[0]['further_sub_id'];
			$this->row->description=$result[0]['description'];
			$this->row->img1=$result[0]['img1'];
			$this->row->img2=$result[0]['img2'];
			$this->row->img3=$result[0]['img3'];
			$this->row->w_price=$result[0]['w_price'];
			$this->row->r_price=$result[0]['r_price'];
			$this->row->stock=$result[0]['stock'];
			$this->row->item_code=$result[0]['item_code'];
			$this->row->sc_code=$result[0]['sc_code'];
			$this->row->url=base_url();
			echo(json_encode($this->row));
			//print_r($result['countries']);
			exit;
			} else {
            redirect('c_login', 'refresh');
        }
 } 
 
 
}
/* End of file c_home.php */
/* Location: ./application/controllers/admin/c_home.php */