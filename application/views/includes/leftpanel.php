<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
			<?php
			$dashbord='';
			$packages='';
			$special_offers='';
			$users='';
			$country='';
			$cms='';
			$c_image_manager='';
			$news='';
			$events='';
			$c_photo_manager='';
			if( $this->uri->segment(3)=='c_home'){
			$dashbord='class="active"';
			}elseif($this->uri->segment(3)=='c_packages'){
			$packages='class="active"';
			}elseif($this->uri->segment(3)=='c_special_offers'){
			$special_offers='class="active"';
			}elseif($this->uri->segment(3)=='c_users'){
			$users='class="active"';
			}elseif($this->uri->segment(3)=='c_country'){
			$country='class="active"';
			}
			elseif($this->uri->segment(3)=='c_cms'){
			$cms='class="active"';
			}
			elseif($this->uri->segment(3)=='c_image_manager'){
			$c_image_manager='class="active"';
			}elseif($this->uri->segment(3)=='c_news'){
			$news='class="active"';
			}elseif(($this->uri->segment(3)=='c_events')||($this->uri->segment(3)=='c_event_manager')){
			$events='class="active"';
			}elseif($this->uri->segment(3)=='c_photo_manager'){
			$c_photo_manager='class="active"';
			}
			
			?>
            	<li class="nav-header">Navigation</li>
                <li <?php echo($dashbord);?>><a href="<?php echo base_url('en/admin/c_home');?>"><span class="iconfa-laptop"></span> Dashboard</a></li>
				 <li <?php echo($c_image_manager);?>><a href="<?php echo base_url('en/admin/c_image_manager');?>"><span class="iconfa-picture"></span> Image Manager</a></li>
				<?php /*?> <li class="dropdown"><a><span class="iconfa-picture"></span> Image Manager</a>
				    <ul>
                    	<li><a href="<?php echo base_url('en/admin/c_image_manager');?>">Gallery</a></li>
                        <li><a href="<?php echo base_url('en/admin/c_country');?>">Best Photos</a></li>
                    </ul>
				</li><?php */?>
				<li <?php echo($packages);?>><a href="<?php echo base_url('en/admin/c_packages');?>"><span class="iconfa-briefcase"></span> Packages</a></li>
				<li <?php echo($special_offers);?>><a href="<?php echo base_url('en/admin/c_special_offers');?>"><span class="iconfa-th-list"></span> Special Offers</a></li>
				<li <?php echo($users);?>><a href="<?php echo base_url('en/admin/c_users');?>"><span class="iconfa-user"></span> Users</a></li>
				<li <?php echo($country);?>><a href="<?php echo base_url('en/admin/c_country');?>"><span class="iconfa-font"></span> Country</a></li>
				<li <?php echo($cms);?>><a href="<?php echo base_url('en/admin/c_cms');?>"><span class="iconfa-file"></span> Cms</a></li>
				<li <?php echo($news);?>><a href="<?php echo base_url('en/admin/c_news');?>"><span class="iconfa-book"></span> News</a></li>
				<li <?php echo($events);?>><a href="<?php echo base_url('en/admin/c_events');?>"><span class="iconfa-calendar"></span> Events</a></li>
			<li <?php echo($c_photo_manager);?>><a href="<?php echo base_url('en/admin/c_photo_manager');?>"><span class="iconfa-camera"></span>Best Photos</a></li>
				
				
               <!-- <li><a href="buttons.html"><span class="iconfa-hand-up"></span> Buttons &amp; Icons</a></li>
                <li class="dropdown"><a ><span class="iconfa-pencil"></span> Forms</a>
                	<ul>
                    	<li><a href="forms.html">Form Styles</a></li>
                        <li><a href="wizards.html">Wizard Form</a></li>
                        <li><a href="wysiwyg.html">WYSIWYG</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a ><span class="iconfa-briefcase"></span> UI Elements &amp; Widgets</a>
                	<ul>
                    	<li><a href="elements.html">Theme Components</a></li>
                        <li><a href="bootstrap.html">Bootstrap Components</a></li>
                        <li><a href="boxes.html">Headers &amp; Boxes</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a ><span class="iconfa-th-list"></span> Tables</a>
                	<ul>
                    	<li><a href="table-static.html">Static Table</a></li>
                        <li class="dropdown"><a href="table-dynamic.html">Dynamic Table</a></li>
                    </ul>
                </li>
                <li><a href="media.html"><span class="iconfa-picture"></span> Media Manager</a></li>
                <li><a href="typography.html"><span class="iconfa-font"></span> Typography</a></li>
                <li><a href="charts.html"><span class="iconfa-signal"></span> Graph &amp; Charts</a></li>
                <li><a href="messages.html"><span class="iconfa-envelope"></span> Messages</a></li>
                <li><a href="calendar.html"><span class="iconfa-calendar"></span> Calendar</a></li>
                <li class="dropdown"><a ><span class="iconfa-book"></span> Other Pages</a>
                	<ul>
                    	<li><a href="404.html">404 Error Page</a></li>
                        <li><a href="editprofile.html">Edit Profile</a></li>
                        <li><a href="invoice.html">Invoice Page</a></li>
                        <li><a href="discussion.html">Discussion Page</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a ><span class="iconfa-th-list"></span> Three Level Menu Sample</a>
                	<ul>
                    	<li class="dropdown"><a >Second Level Menu</a>
                        <ul>
                            <li><a >Third Level Menu</a></li>
                            <li><a >Another Third Level Menu</a></li>
                        </ul>
                     </li>
                    </ul>
                </li>-->
            </ul>
        </div><!--leftmenu-->
        
    </div>