<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Profiler</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.default.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive-tables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-fileupload.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/isotope.css" type="text/css" />


<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.autogrow-textarea.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/charCount.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/ui.spinner.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/responsive-tables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.alerts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/forms.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>
<?php
if($profile)
{
	foreach($profile as $row)
							{
							$name = $row['name'];
							$img1 = $row['img1'];
							$img2 = $row['img2'];							
							}
}
?>
<div class="mainwrapper">
    
    <div class="header">
        <div class="logo">
            <a href="<?php echo base_url('c_home/profile');?>"><?php /*?><img src="<?php echo base_url();?>assets/images/logo.png" alt="" /><?php */?></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">
                <li class="right">
                    <div class="userloggedinfo">
					<?php
				if(isset($img1))
				{
				?>
			  <img src="<?php echo base_url("uploads/img/$img1");?>" width="80" alt="" />
			  <?php
				}
				else
				{
			  ?>
			  <img src="<?php echo base_url("assets/images/photos/thumb1.png");?>" alt="" />
			  <?php
				}
			  ?>
                        
                        <div class="userinfo">
                            <h5><?php if(isset($name)) { echo $name; } ?></h5>
                            <ul>
                                <li><a href="<?php echo base_url('c_form/');?>"><?php if(isset($name)) { ?> Edit Profile<?php } else { ?> Add Profile <?php } ?></a></li>
                                
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
  <?php /*?> <?php include_once('leftpanel.php');?><?php */?>
    <!-- leftpanel -->
    
   
        
       