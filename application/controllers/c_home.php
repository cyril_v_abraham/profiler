<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_home extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_form','',TRUE);
        $this->load->helper('url');
        $this->load->library(array('form_validation','session'));
    }
 
    
 function profile(){
	 
		$result['profile']=$this->m_form->getAllProfile();
		$this->load->view('includes/header',$result);
        $this->load->view('v_profile',$result);
	    $this->load->view('includes/footer');
		
}


 
}
/* End of file c_home.php */
/* Location: ./application/controllers/admin/c_home.php */